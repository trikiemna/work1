//function charts (labels, data,chart){
    
  /*   function getDoC(url){
        var xmlhttp = new XMLHttpRequest();
    
        xmlhttp.onreadystatechange=function(){
          if (this.readyState == 4 && this.status == 200)  {
          doc_ =  this.responseText;
          //alert("1__" + doc_);
           //alert("__" + doc);
               doc = JSON.parse(doc_);
               //doc = myObj;
           
               //var res= CeS(doc);
               
                   }};
                   
               xmlhttp.open("GET", url,false);
               xmlhttp.send();
               
         //  alert("2__"+doc_);
           return doc;
           
           }
        function pie(doc)
    {
    //alert("ces" + doc);
 */

let ctx = document.getElementById('chart1').getContext('2d');
let labels = ['Coût réel'];
let colorHex = ['rgba(250, 0, 0, 1)','rgba(250, 250, 250, 250)'];

let chart1 = new Chart(ctx, {
  type: 'pie',
  data: {labels: labels,
    datasets: [{
	
      data: [25, 100-25],
      backgroundColor: colorHex,
	   borderColor: 'rgb(0,0,0)',
	  borderWidth: 1
    }],
    
  },
  options: {
    responsive: true,
   /*  legend: {
      position: 'bottom'
    }, */
   plugins: {
      datalabels: {
        color: '#fff',
        anchor: 'end',
        align: 'start',
        offset: 1,
        borderWidth: 1.5,
        borderColor: '#fff',
        borderRadius: 10,
        backgroundColor: (context) => {
          return context.dataset.backgroundColor;
        },
        font: {
          weight: 'bold',
          size: '10'
        },
        formatter: (value) => {
          return value + ' %';
        }
      }
    }
  }
});
    /* }
});

return true;
}


         function affichpie(){

    var doc =  getDoC(url);
//	 alert("__"+url +"__"+doc);
var pieAffich=   pie(doc);
       }
    
    var url = "http://localhost/demo/doc.txt";
var doc_ = "doc";
var doc = null;
//console.log ((ParseInt("doc.janvier",10) + ParseInt("doc.fevrier",10)+ ParseInt("doc.mars",10) + ParseInt("doc.avril",10)+ ParseInt("doc.mai",10) + ParseInt("doc.juin ",10)+ ParseInt("doc.juillet",10) + doc));
       affichpie();


//return charts(); */




let ctx1 = document.getElementById('chart2').getContext('2d');
let labels1 = ['charge de travail achevé'];
let colorHex1 = ['rgba(250, 0, 0, 1)','rgba(250, 250, 250, 250)'];

let chart2 = new Chart(ctx1, {
  type: 'pie',
  data: {labels: labels1,
    datasets: [{
	
      data: [30,100-30],
      backgroundColor: colorHex1,
	   borderColor: 'rgb(0,0,0)',
	  borderWidth: 1
    }],
    
  },
  options: {
    responsive: true,
   /*  legend: {
      position: 'bottom'
    }, */
   plugins: {
      datalabels: {
        color: '#fff',
        anchor: 'end',
        align: 'start',
        offset: 1,
        borderWidth: 1.5,
        borderColor: '#fff',
        borderRadius: 10,
        backgroundColor: (context) => {
          return context.dataset.backgroundColor;
        },
        font: {
          weight: 'bold',
          size: '10'
        },
        formatter: (value) => {
          return value + ' %';
        }
      }
    }
  }
});

let ctx2 = document.getElementById('chart3').getContext('2d');
let labels2 = ['Temps de travail achevé'];
let colorHex2 = ['rgba(250, 0, 0, 1)','rgba(250, 250, 250, 250)'];

let chart3 = new Chart(ctx2, {
  type: 'pie',
  data: {labels: labels2,
    datasets: [{
	
      data: [70, 100-70],
      backgroundColor: colorHex2,
	   borderColor: 'rgb(0,0,0)',
	  borderWidth: 1
	  
    }],
    
  },
  options: {
    responsive: true,
   /*  legend: {
      position: 'bottom'
    }, */
   plugins: {
      datalabels: {
        color: '#fff',
        anchor: 'end',
        align: 'start',
        offset: 1,
        borderWidth: 1.5,
        borderColor: '#fff',
        borderRadius: 10,
        backgroundColor: (context) => {
          return context.dataset.backgroundColor;
        },
        font: {
          weight: 'bold',
          size: '10'
        },
        formatter: (value) => {
          return value + ' %';
        }
      }
    }
  }
});

   
let ctx3 = document.getElementById('chart4').getContext('2d');
let labels3 = ['Physique achevé'];
let colorHex3 = ['rgba(250, 0, 0, 1)','rgba(250, 250, 250, 250)'];

let chart4 = new Chart(ctx3, {
  type: 'pie',
  data: {labels: labels3,
    datasets: [{
	
      data: [50, 50],
      backgroundColor: colorHex3,
	  borderColor: 'rgb(0,0,0)',
	  borderWidth: 1
    }],
    
  },
  options: {
    responsive: true,
   /*  legend: {
      position: 'bottom'
    }, */
   plugins: {
      datalabels: {
        color: '#fff',
        anchor: 'end',
        align: 'start',
        offset: 1,
        borderWidth: 1.5,
        borderColor: '#fff',
        borderRadius: 10,
        backgroundColor: (context) => {
          return context.dataset.backgroundColor;
        },
        font: {
          weight: 'bold',
          size: '10'
        },
        formatter: (value) => {
          return value + ' %';
        }
      }
    }
  }
});


let ctx4 = document.getElementById('chart5').getContext('2d');
let labels4 = [ 'IPD'];
let colorHex4 = ['rgba(250, 0, 0, 1)','rgba(250, 250, 250, 250)'];

let chart5 = new Chart(ctx4, {
  type: 'pie',
  data: {labels: labels4,
    datasets: [{
	
      data: [0.8,0.2],
      backgroundColor: colorHex4,
	   borderColor: 'rgb(0,0,0)',
	  borderWidth: 1
    }],
    
  },
  options: {
    responsive: true,
   /*  legend: {
      position: 'bottom'
    }, */
   plugins: {
      datalabels: {
        color: '#fff',
        anchor: 'end',
        align: 'start',
        offset: 1,
        borderWidth: 1.5,
        borderColor: '#fff',
        borderRadius: 10,
        backgroundColor: (context) => {
          return context.dataset.backgroundColor;
        },
        font: {
          weight: 'bold',
          size: '10'
        },
        formatter: (value) => {
          return value + ' %';
        }
      }
    }
  }
});

let ctx5 = document.getElementById('chart6').getContext('2d');
let labels5 = ['IPC'];
let colorHex5 = ['rgba(250, 0, 0, 1)','rgba(250, 250, 250, 250)'];

let chart6 = new Chart(ctx5, {
  type: 'pie',
  data: {labels: labels5,
    datasets: [{
	
      data: [0.25, 1-0.25],
      backgroundColor: colorHex5,
	   borderColor: 'rgb(0,0,0)',
	  borderWidth: 1
    }],
    
  },
  options: {
    responsive: true,
   /*  legend: {
      position: 'bottom'
    }, */
   plugins: {
      datalabels: {
        color: '#fff',
        anchor: 'end',
        align: 'start',
        offset: 1,
        borderWidth: 1.5,
        borderColor: '#fff',
        borderRadius: 10,
        backgroundColor: (context) => {
          return context.dataset.backgroundColor;
        },
        font: {
          weight: 'bold',
          size: '10'
        },
        formatter: (value) => {
          return value + ' %';
        }
      }
    }
  }
});

let ctx6 = document.getElementById('chart7').getContext('2d');
let labels6 = ['IPAP'];
let colorHex6 = ['rgba(250, 0, 0, 1)','rgba(250, 250, 250,250)'];

let chart7 = new Chart(ctx6, {
  type: 'pie',
  data: {labels: labels6,
    datasets: [{
	
      data: [30, 70],
      backgroundColor: colorHex6,
	   borderColor: 'rgb(0,0,0)',
	  borderWidth: 1
    }],
    
  },
  options: {
    responsive: true,
   /*  legend: {
      position: 'bottom'
    }, */
   plugins: {
      datalabels: {
        color: '#fff',
        anchor: 'end',
        align: 'start',
        offset: 1,
        borderWidth: 1.5,
        borderColor: '#fff',
        borderRadius: 10,
        backgroundColor: (context) => {
          return context.dataset.backgroundColor;
        },
        font: {
          weight: 'bold',
          size: '10'
        },
        formatter: (value) => {
          return value + ' %';
        }
      }
    }
  }
});



//json test

function getDoc(url){
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange=function(){
      if (this.readyState == 4 && this.status == 200)  {
      doc_ =  this.responseText;
      alert("1__" + doc_);
       //alert("__" + doc);
           doc = JSON.parse(doc_);
           //doc = myObj;
       
           //var res= CeS(doc);
           
               }};
               
           xmlhttp.open("GET", url,false);
           xmlhttp.send();
           
       alert("2__"+doc_);
       return doc;
       
       }
    function CeS(doc)
{
alert("ces" + doc);
//function linecharts (chart,label,data){
      var ctx9 = document.getElementById('myChart9');
var myChart9 = new Chart(ctx9, {
   type: 'line',
   data: {
       labels: ['janvier', 'fevrier', 'mars','avril','mai','juin','juillet'],
        datasets: [{
           label: 'VP',
           data: [doc.janvier, doc.fevrier, doc.mars, doc.avril, doc.mai, doc.juin, doc.juillet],
           //backgroundColor: ['rgba(255, 99, 132, 0.2)'],
           borderColor: ['rgba(255, 99, 132, 1)'],
           borderWidth: 1
       }]
   },
   options: {}
});
return true;
}


         function affich(){

    var doc =  getDoc(url);
//	 alert("__"+url +"__"+doc);
var csAffich=   CeS(doc);
       }
    
    var url = "http://localhost/demo/doc.txt";
var doc_ = "doc";
var doc = null;
       affich();


       
var ctx10 = document.getElementById('myChart10');
     var myChart10 = new Chart(ctx10, {
    type: 'line',
    data: {
        labels: ['janvier', 'fevrier', 'mars','avril','mai','juin','juillet'],
         datasets: [{
            label: 'VP',
            data: [20,5,12,6,8,1,0],
            //backgroundColor: ['rgba(255, 99, 132, 0.2)'],
            borderColor: ['rgba(255, 99, 132, 1)'], 
            borderWidth: 1
        }]
    },
    options: {}
});
/*var ctx10 = document.getElementById('myChart11').getContext('2d');
     var myChart10 = new Chart(ctx10, {
     type: 'horizontalBar',
        labels: ['janvier', 'fevrier', 'mars','avril','mai','juin','juillet'],
        data:{
            
        datasets: [{
            label: 'IPD',
            backgroundColor:['rgba(255, 99, 132, 0.2)'] ,
            borderColor: ['rgba(255, 99, 132, 1)'],
            borderWidth: 1,
            data: [15]
        }]},
        options: {}
});*/
